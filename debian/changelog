x2goclient (4.1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase 1002_fix-ftbfs-on-non-linux.patch.
    + Update 1002_fix-ftbfs-on-non-linux.patch. Spot another location where to
      drop inclusion of sys/mount.h.
  * debian/control:
    + Bump Standards-Version: to 4.1.3. No changes needed.
  * debian/{control,compat}:
    + Bump DH version level to 11.
  * debian/watch:
    + Use secure URL to retrieve upstream's source tarball.
  * debian/changelog: White-space clean-up.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 19 Feb 2018 22:49:12 +0100

x2goclient (4.1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.1. No changes needed.
  * debian/patches:
    + Rebase 1002_fix-ftbfs-on-non-linux.patch. Drop non-required sys/mount.h
      include.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 03 Nov 2017 09:54:27 +0100

x2goclient (4.1.0.1-2) unstable; urgency=medium

  * debian/control:
    + Comply with policy version 4.1.0; Switch from Priority: extra to Priority:
      optional. Thanks to Unit193 for pointing that out.
  * debian/patches:
    + Add 1002_fix-ftbfs-on-non-linux.patch. Fix FTBFS on Debian GNU/kFreeBSD
      and Debian GNU/Hurd.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 01 Oct 2017 20:14:31 +0200

x2goclient (4.1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.0. No changes needed.
  * debian/{control,rules}:
    + Don't ship dbg:packages manually, switch to automated builds of
      dbgsym:packages.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 17 Sep 2017 19:38:20 +0200

x2goclient (4.1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch:
    + Recognize upstream versions with two (or more) digits in any of the
      release numberlets.
  * debian/control:
    + Bump Standards-Version: to 4.0.0. No changes needed.
  * debian/{control,compat}:
    + Bump DH version level to 10.
  * debian/rules:
    + Enable dh_missing --fail-missing.
  * debian/{rules,*.install}:
    + Use upstream's Makefile for installing files into the bin:packages.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 28 Jun 2017 11:02:10 +0200

x2goclient (4.0.5.2-2) unstable; urgency=medium

  * Rebuild against libssl1.0-dev (via libssh-dev 0.7.3-2).
    (Closes: #844825).
  * debian/control:
    + New maintenance umbrella: Debian Remote Maintainers team.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 30 Nov 2016 08:31:54 +0100

x2goclient (4.0.5.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 3.9.8. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 20 Oct 2016 14:08:17 +0200

x2goclient (4.0.5.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch:
    + Be more strict on the version regexp. Don't find releases for MacOS X.
  * debian/control:
    + Use encrypted URLs in VCS-*: fields.
    + Bump Standards: to 3.9.7. No changes needed.
    + Add versioned B-D: dpkg-dev (>= 1.16.1.1).
  * debian/patches:
    + Drop 1002_man-page-fixes.patch. Applied upstream.
  * debian/copyright:
    + Update years in Copyright: fields.
    + Place license texts at end of file.
  * debian/{compat,control}:
    + Use DH version level 9.
  * debian/rules:
    + Enable all hardening flags.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 12 Apr 2016 13:11:44 +0200

x2goclient (4.0.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 28 Jul 2015 17:54:52 +0200

x2goclient (4.0.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules:
    + Add get-orig-source rule.
    + Adapt override_dh_auto_clean rule to path changes in this upstream
      release.
  * debian/copyright:
    + Update copyright years to latest upstream version. Update src:path
      of the browser plugin code.
  * debian/patches:
    + Drop all 000?_*.patch files. All applied upstream.
        0001_use-QUrl::toPercentEncoding-for-encoding-broker-passwords.patch
        0002_fix-GUI-for-session-profile-subfolders.patch
        0003_use-app-setQuitOnLastWindowClose-false-for-X2GoClient-
	  application.patch
        0004-fix-quoting+escaping-ssh-commands.patch
        0005_really-logout-from-broker-after-logout.patch
    + Modify 1001_deprecated-apache2-config.patch. Only support Apache2.4
      syntax.
    + Add 1002_man-page-fixes.patch. Fix several hyphen-used-as-minus-sign
      issues.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 08 Jun 2015 21:02:58 +0200

x2goclient (4.0.3.1-4) unstable; urgency=medium

  * debian/patches:
    + Add 0005_really-logout-from-broker-after-logout.patch. Really log-off a
      user if --broker-autologoff is used. (Closes: #777314).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 07 Feb 2015 13:03:04 +0100

x2goclient (4.0.3.1-3) unstable; urgency=medium

  * debian/patches:
    + Add 0003_use-app-setQuitOnLastWindowClose-false-for-X2GoClient-
      application.patch. Prevent X2Go Client from exiting abnormally on
      printing incoming printjobs via the print dialog window. (Closes:
      #774907).
    + Add 0004-fix-quoting+escaping-ssh-commands.patch. Fix quotes when calling
      remote commands via SSH (esp. allow same quoting/ escaping style for
      libssh and openSSH+Krb based connections). (Closes: #774998).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 09 Jan 2015 21:57:45 +0100

x2goclient (4.0.3.1-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001_use-QUrl::toPercentEncoding-for-encoding-broker-passwords.patch.
      Correctly encode passwords before sending them to the session broker via
      http(s). (Closes: #773348).
    + Add 0002_fix-GUI-for-session-profile-subfolders.patch. Fix a GUI
      layout/resizing issue for session profiles with profile subfolders.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 06 Jan 2015 05:57:53 +0100

x2goclient (4.0.3.1-1) unstable; urgency=medium

  * New upstream release.
    - Fix (cross-)user desktop sharing / helpdesk functionality (Closes:
      #770963).
    - Split up session profile dialog's "Settings" tab into a "Input/Output"
      tab and a media tab (Closes: #770962).
    - If a session broker is used with X2Go Client, allow configuration of
      audio settings through the broker.
    - Greatly improved man page.
    - Updated (upstream) translation files.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 27 Nov 2014 11:09:10 +0100

x2goclient (4.0.3.0-1) unstable; urgency=medium

  * New upstream release.
    - Provide clipboard mode option for session profiles. (Closes: #714588).
  * debian/copyright:
    + Update one copyright date for debian/*.
  * debian/control:
    + Bump Standards: to 3.9.6.
  * debian/watch:
    + Add cryptographic signature verification for upstream tarball.
  * debian/patches:
    + Add 1001_deprecated-apache2-config.patch. Use IfVersion tag to detect
      installed Apache2 version.
  * debian/README.source:
    + Drop deprecated description (i.e. remove this file).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Oct 2014 13:20:57 +0200

x2goclient (4.0.2.1-1) unstable; urgency=low

  * New upstream version.
  * debian/patches:
    + Drop 1001_fix-ftbfs-arm64.patch. Applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 Aug 2014 18:39:06 +0000

x2goclient (4.0.2.0-2) unstable; urgency=low

  * Tested against libssh 0.6.3. (Closes: #739496).

  * debian/patches:
    + Fix FTBFS on arm64 architecture (and other newer archs). (Closes:
      #747638).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 14 May 2014 11:03:22 +0200

x2goclient (4.0.2.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Raise versioned B-D: libssh (>= 0.5.4-2~).
    + Bump Standards: to 3.9.5. No changes needed.
    + Provide dbg:package for X2Go Plugin.
    + Make sure very old x2goclient-gtk instances get uninstalled when updating
      to X2Go Client 4.0.2.0.
    + Don't directly recommend apache2.
  * debian/rules:
    + Make upstream ChangeLog available for X2Go Client build.
    + Drop manually stripping symbols from libx2goplugin.so.
  * upstream changelog:
    + Stop shipping upstream changelog in debian/ folder. Upstream provides it
      now in their source tarballs.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 09 May 2014 18:57:26 +0200

x2goclient (4.0.1.1-1) unstable; urgency=low

  * New upstream release.
  * /debian/control:
    + Hyphenation fix in long description.
    + Add RDP clients (rdesktop, freerdp-x11) to Recommends: fields (x2goclient,
      x2goplugin).
    + Append comma to multi-line fields (event to the last line).
    + Update LONG_DESCRIPTION texts.
    + Build-Depend on apache2-dev (FIXME later: dh_apache2 and maintenance
      scripts will be moved into the regular debhelper package some time).
    + Alioth-canonicalize Vcs-*: fields.
  * /debian/rules:
    + Add bin:package: x2goclient-dbg.
  * Fix lintian issues:
    + W: x2goplugin-provider:
         apache2-reverse-dependency-uses-obsolete-directory
         etc/apache2/conf.d/x2goplugin.conf
    + W: x2goplugin-provider: non-standard-apache2-configuration-name
         x2goplugin.conf != x2goplugin-provider.conf
    + E: x2goplugin: unstripped-binary-or-object
         usr/lib/mozilla/plugins/libx2goplugin.so
    + I: x2goplugin: binary-has-unneeded-section
         usr/lib/mozilla/plugins/libx2goplugin.so .comment
  * Use dh_apache2 for building bin:package x2goplugin-provider.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 11 Sep 2013 13:37:14 +0000

x2goclient (4.0.1.0-1) unstable; urgency=low

  * New upstream release. (Closes: #682852, #703324).
  * Add bin:package x2goplugin-provider. An example package that shows how to
    set up a site providing session profiles to X2Go Plugin via html based
    configuration.
  * /debian/control:
    + Use my DD mail address in Uploaders: field.
    + Bump Standards: to 3.9.4. No changes needed.
  * /debian/copyright:
    + Use my DD mail address in copyright notice for /debian/*.
    + Add OpenSSL exception.
  * /debian/patches:
    + Drop all patches, all fixes have been incorporated by upstream.
  * /debian/x2goplugin.examples:
    + Drop x2goplugin.html example. Has its own bin:package now.
  * /debian/README.source et al.:
    + Drop merged-in icons/SVGs from package (see #697310). Upstream devs have
      finally fixed their artwork files.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 23 Mar 2013 00:57:07 +0100

x2goclient (3.99.2.1-5) unstable; urgency=low

  * /debian/rules:
    + Undo replacing of /svg/x2gologo.svg. (Closes: #693710).

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 04 Mar 2013 06:03:26 +0100

x2goclient (3.99.2.1-4) unstable; urgency=low

  * /debian/007_fix-desktop-file-version.patch:
    - Add missing patch header.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Sat, 19 Jan 2013 11:35:40 +0100

x2goclient (3.99.2.1-3) unstable; urgency=low

  * /debian/README.source (new):
    + Add information about upstream changelog.
    + Add information about the icon update and replacement mechanism.
  * Replace some faulty icons by icons from an icon update set provided by
    upstream. This fixes bad icon visibility on desktops that use a dark theme.
    (Closes: #693710).
  * /debian/source/include-binaries (new):
    + Explicitly allow to-be-updated-during-build PNG files in /debian folder.
  * /debian/rules:
    + Cleanup build cruft during clean: x2goclient_*.qm files.
  * Add patch: 007_fix-desktop-file-version.patch. Wrong usage of field,
    originally contains upstream version. Replacing that with the version
    of the desktop file format in use (i.e. 1.0).

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Sat, 19 Jan 2013 11:06:05 +0100

x2goclient (3.99.2.1-2) unstable; urgency=low

  * /debian/copyright:
    + Set copyright format to v1.0.
  * /debian/control:
    + Orthographical and grammatical fixes in description texts.
  * Fix patch: 001_hardening-x2goclient.patch, provide CPPFLAGS for
    QMAKE_CFLAGS, as well. Provide CPPFLAGS first.
  * Add patches: 004_allow-login-to-non-sh-users.patch and
    005_xinerama-for-non-sh-users.patch. Fix x2goclient for users that use
    a non-sh shell (e.g. like tcsh) on the X2Go Server. (Closes: #682839).
  * Add patch: 006_fix-x2goplugin.patch. Fixes host name resolving when
    client is run in X2Go Plugin mode.  Fixes session crashes directly
    after session startup. Let the plugin tolerate whitespace at the beginning
    of session profile configs. (Closes: #692555, #696524, #696529).
  * Add example file: x2goplugin.html.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Sat, 22 Dec 2012 21:36:55 +0100

x2goclient (3.99.2.1-1) unstable; urgency=low

  * New upstream release.
  * Adapt watch file rule to upstream tarball naming scheme changes.
  * Use dpkg-buildflags to harden x2goclient and libx2goplugin.so.
  * Add patch: 001_hardening-x2goclient.patch, make upstream Makefile
    draw in QMAKE_* variables for build flags.
  * Add patch: 002_remove-encoding-from-desktop-entry.patch, encoding keys in
    .desktop files have become deprecated (FreeDesktop.org).
  * Add patch: 003_fix-spelling-error-authentication.patch, fix the spelling
    of the word authentication.
  * Also add upstream changelog to bin:package x2goplugin.
  * Do not ship html version of man page anymore
  * Drop AUTHORS file from x2goclient's doc folder.
  * Drop AUTHORS file from x2goplugin's doc folder.
  * Update /debian/copyright: fix licensing years, add license for
    Makefile.docupload.
  * Enable --parallel build options in /debian/rules.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Tue, 12 Jun 2012 00:15:18 +0200

x2goclient (3.99.2.0-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.9.3.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Wed, 04 Apr 2012 11:52:07 +0200

x2goclient (3.99.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Thu, 08 Mar 2012 08:37:14 +0100

x2goclient (3.99.1.0-1) unstable; urgency=low

  * New upstream release.
  * License change (LGPL-2 -> GPL-2+) by upstream on files:
    - sshmasterconnection.*
    - sshprocess.h
  * Update upstream changelog.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Wed, 22 Feb 2012 21:51:45 +0100

x2goclient (3.99.0.5-1) unstable; urgency=low

  * Initial package build (closes: #655602).
  * Add watch file for upstream tarball location.
  * Use source format 3.0 (quilt).

  [ Reinhard Tartler ]
  * Improve copyright file.

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 27 Jan 2012 16:01:30 +0100
